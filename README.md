Chuck-Jokes-client
This is the solution for the case at FrontMen.

Getting Started
No special CLI or any type of black magic is needed, so go ahead and clone this repository.

### Quick Start

Install dependencies

```
$ npm install
```

And run as development

```
$ npm start
```

Check out `http://localhost:8080`

## Built With

- [Webpack](https://webpack.js.org/) - Module bundler
- [React](https://reactjs.org/) - UI Javascript library
- [Redux](https://redux.js.org/) - Predictable state container
- [Tailwind CSS](https://tailwindcss.com/) - Utility-First CSS Framework

Among others libraries, see the [package.json](package.json) file.

## Author

- **Diego Toro**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
