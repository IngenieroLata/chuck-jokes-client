import axios from './axios';

export function getJokes() {
  return axios
    .get('/jokes/random/10')
    .then(results => {
      return results.data;
    })
    .then(({ type, value }) => {
      if (type !== 'success') {
        throw new Error(value);
      }
      return value;
    });
}

export function getJoke() {
  return axios
    .get('/jokes/random/1')
    .then(results => {
      return results.data;
    })
    .then(({ type, value }) => {
      if (type !== 'success') {
        throw new Error(value);
      }
      return value;
    });
}
