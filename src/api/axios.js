import axios from 'axios';

const { CancelToken } = axios;
const source = CancelToken.source();

const instance = axios.create({
  baseURL: 'http://localhost:3000',
  cancelToken: source.token,
});

export default instance;
