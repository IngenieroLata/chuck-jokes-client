import React from 'react';
import { NavLink } from 'react-router-dom';

import Logo from './logo.svg';

const Layout = ({ children }) => (
  <div className="h-full">
    <div className="bg-white">
      <div className="py-5 text-center">
        <Logo />
      </div>
      <div className="bg-black">
        <ul className="list-reset w-3/5	mx-auto p-0 flex">
          <li className="flex-1">
            <NavLink to="/jokes" className="anchor">
              <div className="box-link">
                <span className="link">Jokes</span>
              </div>
            </NavLink>
          </li>
          <li className="flex-1">
            <NavLink to="/favorites" className="anchor">
              <div className="box-link">
                <span className="link">Favorites</span>
              </div>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
    <div className="w-3/5 mx-auto bg-black">{children}</div>
  </div>
);

export default Layout;
