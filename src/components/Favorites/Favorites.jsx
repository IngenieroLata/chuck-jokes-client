import React from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';

import Joke from '../Joke';
import InfoText from '../InfoText';
import {
  updateJoke,
  startRandomFill,
  stopRandomFill,
} from '../../redux/modules/jokes';

const Favorites = ({
  jokes,
  updateJoke,
  timer,
  startRandomFill,
  stopRandomFill,
}) => {
  const callButton = !timer.status ? (
    <button className="button mt-4" onClick={() => startRandomFill()}>
      Start filling favorites!!
    </button>
  ) : (
    <div className="text-white text-xl">
      <p>Loading Joke in: {timer.seconds} secs </p>
      <p>or</p>

      <button className="button mt-4" onClick={() => stopRandomFill()}>
        Stop! No More Jokes!!
      </button>
    </div>
  );

  if (!jokes.length) {
    return (
      <div>
        <InfoText>
          <p>Chuck has infinite favorites...</p>
          <p>You only 10, choose wisely</p>
        </InfoText>
        {callButton}
      </div>
    );
  }

  return (
    <div className="mt-8">
      {jokes.length < 10 && callButton}

      {jokes.map(joke => (
        <Joke
          key={joke.id}
          {...joke}
          onChange={e => updateJoke(joke.id, e.target.checked)}
        />
      ))}
    </div>
  );
};

export const getVisibleJokes = createSelector([state => state.jokes], jokes =>
  Object.keys(jokes)
    .filter(id => jokes[id].favorite)
    .map(id => jokes[id])
);

const mapStateToProps = state => ({
  jokes: getVisibleJokes(state),
  timer: state.timer,
});

export default connect(
  mapStateToProps,
  { updateJoke, startRandomFill, stopRandomFill }
)(Favorites);
