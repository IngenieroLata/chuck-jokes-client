import React from 'react';

const InfoText = ({ text, children }) => (
  <div className="text-center text-grey-darker text-4xl px-20 pt-20">
    {text || children || ''}
  </div>
);

export default InfoText;
