import React from 'react';
import unfancy from 'string-unfancy';

import Favorite from './Favorite';

const Joke = ({ id, joke, favorite, onChange }) => (
  <div className="my-8 pb-2 border-b-2 border-white">
    <div className="bg-white flex">
      <div className="py-6 px-4">
        <Favorite idName={id} onChange={onChange} value={favorite} />
      </div>
      <p className="flex-1 text-2xl text-justify py-8 px-4">{unfancy(joke)}</p>
    </div>
  </div>
);

export default Joke;
