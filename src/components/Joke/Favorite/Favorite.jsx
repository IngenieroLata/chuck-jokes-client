import React from 'react';

const Favorite = ({ idName, value, onChange }) => (
  <div className="favorite-wrapper">
    <input type="checkbox" id={idName} onChange={onChange} checked={!!value} />
    <label htmlFor={idName}>
      <svg width="50px" height="50px">
        <path d="M25.5,47 C14.7304474,47 6,38.2695526 6,27.5 C6,16.7304474 14.7304474,8 25.5,8 C36.2695526,8 45,16.7304474 45,27.5 C45,38.2695526 36.2695526,47 25.5,47 Z M25.5,42.5 C33.7842712,42.5 40.5,35.7842712 40.5,27.5 C40.5,19.2157288 33.7842712,12.5 25.5,12.5 C17.2157288,12.5 10.5,19.2157288 10.5,27.5 C10.5,35.7842712 17.2157288,42.5 25.5,42.5 Z" />
        <g className="star">
          <polygon points="25 36.75 10.3053687 48.2254249 16.6782555 30.7038987 1.22358709 20.2745751 19.856879 20.9211013 25 3 30.143121 20.9211013 48.7764129 20.2745751 33.3217445 30.7038987 39.6946313 48.2254249" />
          <circle cx="25" cy="3" r="2" />
          <circle cx="48" cy="20" r="2" />
          <circle cx="3" cy="20" r="2" />
          <circle cx="40" cy="47" r="2" />
          <circle cx="11" cy="47" r="2" />
        </g>
      </svg>
    </label>
  </div>
);

export default Favorite;
