import React, { PureComponent } from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';

import Joke from '../Joke';
import InfoText from '../InfoText';
import { getJokes, updateJoke } from '../../redux/modules/jokes';

class Jokes extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { error: false };
  }
  componentDidMount() {
    this.props.getJokes();
  }

  onChange(id, e) {
    const { favorites } = this.props;
    const { checked } = e.target;

    if (favorites.length < 10 || !checked) {
      this.setState({ error: false });
      this.props.updateJoke(id, checked);
    } else {
      this.setState({ error: true });
    }
  }

  render() {
    const { jokes } = this.props;
    if (!jokes.length) {
      return <InfoText text="Chuck said no jokes for you" />;
    }
    return (
      <div>
        {this.state.error && (
          <InfoText text="NOPE, Only Chuck has infinite favorites" />
        )}
        {jokes.map(joke => (
          <Joke
            key={joke.id}
            {...joke}
            onChange={this.onChange.bind(this, joke.id)}
          />
        ))}
      </div>
    );
  }
}

export const getVisibleJokes = createSelector(
  [state => state.jokes, state => state.visibleJokes],
  (jokes, jokesIds) => jokesIds.map(id => jokes[id])
);

export const getFavorites = createSelector([state => state.jokes], jokes =>
  Object.keys(jokes).filter(id => jokes[id].favorite)
);

const mapStateToProps = state => ({
  jokes: getVisibleJokes(state),
  favorites: getFavorites(state),
});

export default connect(
  mapStateToProps,
  { getJokes, updateJoke }
)(Jokes);
