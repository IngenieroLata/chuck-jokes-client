import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { hot } from 'react-hot-loader';

import Layout from '../Layout';
import Jokes from '../Jokes';
import Favorites from '../Favorites';

const App = () => (
  <BrowserRouter>
    <Layout>
      <Switch>
        <Redirect exact from="/" to="/jokes" />
        <Route path="/jokes" component={Jokes} />
        <Route path="/favorites" component={Favorites} />
      </Switch>
    </Layout>
  </BrowserRouter>
);

export default hot(module)(App);
