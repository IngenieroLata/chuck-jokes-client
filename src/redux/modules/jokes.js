import API from '../../api';
import { LoadVisibleJokes } from './visible-jokes';
import { startTimer, stopTimer } from './timer';

/* Constants */
export const LOAD = 'chuck/jokes/LOAD';
export const UPDATE = 'chuck/jokes/UPDATE';

/* Reducers */
export default function reducer(state = {}, action) {
  switch (action.type) {
    case LOAD:
      const { payload } = action;

      const jokes = payload.reduce(
        (norm, joke) => ({
          ...norm,
          [joke.id]: joke,
        }),
        {}
      );

      return {
        ...state,
        ...jokes,
      };
    case UPDATE:
      const {
        payload: { id, value },
      } = action;

      const joke = state[id];
      if (!joke) {
        return state;
      }

      return {
        ...state,
        [id]: {
          ...joke,
          favorite: value,
        },
      };
    default:
      return state;
  }
}

/* Action Creatores */
export const updateJoke = (id, value) => ({
  type: UPDATE,
  payload: { id, value },
});

const LoadJokes = payload => ({
  type: LOAD,
  payload,
});

export const startRandomFill = () => dispatch => {
  dispatch(startTimer(getFavoriteRandomJoke));
};

export const stopRandomFill = () => dispatch => {
  dispatch(stopTimer());
};

const getFavoriteRandomJoke = () => (dispatch, getState) => {
  API.getJoke()
    .then(jokes => jokes.map(joke => ({ ...joke, favorite: true })))
    .then(jokes => dispatch(LoadJokes(jokes)))
    .then(() => {
      const { jokes } = getState();

      const favorites = Object.keys(jokes).filter(id => jokes[id].favorite);
      if (favorites.length < 10) {
        dispatch(startTimer(getFavoriteRandomJoke));
      } else {
        dispatch(stopTimer());
      }
    })
    .catch(err => console.log(err.message));
};

export const getJokes = () => dispatch => {
  API.getJokes()
    .then(jokes => {
      dispatch(LoadJokes(jokes));
      dispatch(LoadVisibleJokes(jokes));
    })
    .catch(err => console.log(err.message));
};
