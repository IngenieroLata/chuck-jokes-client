import { combineReducers } from 'redux';

import timer from './timer';
import jokes from './jokes';
import visibleJokes from './visible-jokes';

export default combineReducers({ jokes, visibleJokes, timer });
