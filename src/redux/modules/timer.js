const START_TIMER = 'chuck/timer/START_TIMER';
const STOP_TIMER = 'chuck/timer/STOP_TIMER';
const TICK = 'chuck/timer/TICK';

let interval = null;
const jump = 100;
const seconds = 2;
const defaultStatus = { status: false };

export default function(state = defaultStatus, action) {
  switch (action.type) {
    case START_TIMER:
      return { ...state, seconds, status: true };
    case STOP_TIMER:
      return { ...state, seconds, status: false };
    case TICK:
      return {
        ...state,
        seconds: (state.seconds - jump / 1000).toFixed(1),
      };
    default:
      return state;
  }
}

const stopAction = () => ({
  type: STOP_TIMER,
});

export const startTimer = call => dispatch => {
  dispatch({ type: START_TIMER });
  clearInterval(interval);
  interval = setInterval(() => {
    dispatch(tick(call));
  }, jump);
};

export const stopTimer = () => dispatch => {
  clearInterval(interval);
  dispatch(stopAction());
};

export const tick = call => (dispatch, getState) => {
  dispatch({ type: TICK });

  const { timer } = getState();
  if (timer.seconds === '0.0') {
    clearInterval(interval);
    dispatch(call());
  }
};
