export const LOAD = 'chuck/visibleJokes/LOAD';

/* Reducer */
export default function reducer(state = [], action) {
  switch (action.type) {
    case LOAD:
      const { payload } = action;

      return payload.map(joke => joke.id);
    default:
      return state;
  }
}

/* Action Creatores */
export const LoadVisibleJokes = payload => ({
  type: LOAD,
  payload,
});
