const postcssPresetEnv = require('postcss-preset-env');
const tailwindcss = require('tailwindcss');
const purgecss = require('@fullhuman/postcss-purgecss');

let plugins = [];

if (process.env.NODE_ENV === 'production') {
  plugins = [
    postcssPresetEnv(),
    tailwindcss('./tailwind.js'),
    purgecss({
      content: ['./src/App.css', './src/**/*.jsx'],
    }),
    require('autoprefixer'),
  ];
} else {
  plugins = [tailwindcss('./tailwind.js')];
}

module.exports = {
  ident: 'postcss',
  plugins,
};
